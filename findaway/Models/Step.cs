﻿namespace findaway.Models {
	public class Step {
		public bool correct { get; set; }

		public Step(bool correct) {
			this.correct = correct;
		}
	}
}