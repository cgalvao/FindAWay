﻿$(document).ready(function () {
    handleClick();
});

function handleClick() {
    $(".option").click(function (e) {
        e.preventDefault();
        const data = {
            step: this.dataset.step,
            option: this.dataset.option
        };
        $.ajax({
            url: "/FindAWay/Jogo",
            type: "POST",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            datatype: "json"
        }).done(function (data) {
            $('#jogo').html(data);
            handleClick();
            $('.option:has(.correctOption)').addClass('correctOption');
        }).fail(function (a, v, e) {
        });
        return false;
    });
}