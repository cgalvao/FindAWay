﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using findaway.Models;
using System.Net.Http;
using System.Collections;
using Newtonsoft.Json;

namespace findaway.Controllers
{
    public class FindAWayController : Controller
    {

		enum Dificulty {
			EasySteps = 3,
			EasyOptions = 3,
			NormalSteps = 5,
			NormalOptions = 4,
			HardSteps = 7,
			HardOptions = 5
		}

		private int maxOptions;
		private int maxSteps;

		private List<List<Step>> possibleSteps = new List<List<Step>>();
		private List<Form> gameSession = new List<Form>();
		private bool terminouJogo = false;

		[HttpPost]
		[ProducesResponseType(200, Type = typeof(Boolean))]
		public IActionResult Jogo([FromBody] Form form) {
			try {
				possibleSteps = JsonConvert.DeserializeObject<List<List<Step>>>(HttpContext.Session.GetString("steps"));
				gameSession = JsonConvert.DeserializeObject<List<Form>>(HttpContext.Session.GetString("gameSession"));
				validarForm(form);
				gameSession.Add(form);
				validarFimDeJogo();
				LoadSteps();
				LoadGameSession();
				return PartialView();
			} catch ( Exception e ) {
				return StatusCode(500);
			}
		}

		[Route("")]
		[Route("FindAWay")]
		[Route("FindAWay/Index")]
		[Route("FindAWay/Normal")]
		public IActionResult Index() {
			GenerateSteps(Dificulty.NormalSteps, Dificulty.NormalOptions);
			LoadSteps();
			LoadGameSession();
			return View();
		}

		[Route("FindAWay/Easy")]
		public IActionResult Easy() {
			GenerateSteps(Dificulty.EasySteps, Dificulty.EasyOptions);
			LoadSteps();
			LoadGameSession();
			return View("Index");
		}

		[Route("FindAWay/Hard")]
		public IActionResult Hard() {
			GenerateSteps(Dificulty.HardSteps, Dificulty.HardOptions);
			LoadSteps();
			LoadGameSession();
			return View("Index");
		}

		private void GenerateSteps(Dificulty dificultySteps, Dificulty dificultyOptions) {
			possibleSteps = new List<List<Step>>();
			maxSteps = (int) dificultySteps;
			maxOptions = (int) dificultyOptions;
			int correctOption = RandomNumber(0, maxOptions);
			for (var currentStep = 0; currentStep < maxSteps; currentStep++ ) {
				List<Step> possibleOptions = new List<Step>();
				for (var currentOption = 0; currentOption < maxOptions; currentOption++ ) {
					bool isCorrectOption = correctOption == currentOption;
					possibleOptions.Add(new Step(isCorrectOption));
				}
				possibleSteps.Add(possibleOptions);
				correctOption = RandomNumber(nextMinValue(correctOption), nextMaxValue(correctOption));
			}
		}

		private static int RandomNumber(int min, int max) {
			Random random = new Random();
			return random.Next(min, max);
		}

		private int nextMinValue(int correctOption) {
			return Math.Max(0, (correctOption - 1));
		}

		private int nextMaxValue(int correctOption) {
			return Math.Min(maxOptions, (correctOption + 2));
		}

		private void LoadSteps() {
			ViewBag.Steps = possibleSteps;
			HttpContext.Session.SetString("steps", JsonConvert.SerializeObject(possibleSteps));
		}

		private void LoadGameSession() {
			ViewBag.GameSession = gameSession;
			ViewBag.terminouJogo = terminouJogo;
			HttpContext.Session.SetString("gameSession", JsonConvert.SerializeObject(gameSession));
		}

		private void validarForm(Form form) {
			if ( terminouJogo || form.Step != gameSession.Count ) {
				throw new ArgumentException();
			}
			if ( !possibleSteps[form.Step][form.Option].correct ) {
				terminouJogo = true;
			}
		}

		private void validarFimDeJogo() {
			if (gameSession.Count >= possibleSteps.Count ) {
				terminouJogo = true;
			}
		}

		public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
